package br.com.audora.organograma.controllers;

import br.com.audora.organograma.dtos.ConsultarPessoaPorNomeDTO;
import br.com.audora.organograma.models.*;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import lombok.SneakyThrows;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/organograma/pessoas")
public class OrganogramaPessoasController {

    @PersistenceContext
    private EntityManager entityManager;

    @GetMapping("/consultar-por-nome/{nome:.*}")
    public ResponseEntity<List<ConsultarPessoaPorNomeDTO>> consultarPessoaPorNome(@PathVariable("nome") String nome) {
        List<Node> nodes = consultarNodesNoOrganograma(nome);

        return ResponseEntity.ok(transformarEmConsultarPessoaPorNomeDTO(nodes));
    }

    private List<Node> consultarNodesNoOrganograma(String texto) {
        String hql = """
                select n
                  from Node n
                 where n.vinculo.pessoa.nome like :texto
                   and n.organograma.ativa = true
                    """;
        return entityManager
                .createQuery(hql, Node.class)
                .setParameter("texto", "%" + texto + "%")
                .getResultList();
    }

    private List<ConsultarPessoaPorNomeDTO> transformarEmConsultarPessoaPorNomeDTO(List<Node> nodes) {

        Map<ConsultarPessoaPorNomeDTO.Unidade, List<ConsultarPessoaPorNomeDTO.PessoaDepartamentoFuncao>> map = new HashMap<>();

        for (Node node : nodes) {
            Pair<ConsultarPessoaPorNomeDTO.Unidade, ConsultarPessoaPorNomeDTO.PessoaDepartamentoFuncao> par = getUnidade(node);
            if (!map.containsKey(par.getKey())) {
                List<ConsultarPessoaPorNomeDTO.PessoaDepartamentoFuncao> pessoaDepartamentoFuncaos = new ArrayList<>();
                pessoaDepartamentoFuncaos.add(par.getValue());
                map.put(par.getKey(), pessoaDepartamentoFuncaos);
            } else {
                List<ConsultarPessoaPorNomeDTO.PessoaDepartamentoFuncao> pessoaDepartamentoFuncaos = map.get(par.getKey());
                pessoaDepartamentoFuncaos.add(par.getValue());
                map.put(par.getKey(), pessoaDepartamentoFuncaos);
            }
        }

        List<ConsultarPessoaPorNomeDTO> dto = new ArrayList<>();

        for (Map.Entry<ConsultarPessoaPorNomeDTO.Unidade, List<ConsultarPessoaPorNomeDTO.PessoaDepartamentoFuncao>> entry : map.entrySet()) {

            entry.getKey().setPessoaDepartamentoFuncaos(entry.getValue());

            ConsultarPessoaPorNomeDTO consultarPessoaPorNomeDTO = new ConsultarPessoaPorNomeDTO();
            consultarPessoaPorNomeDTO.setUnidade(entry.getKey());

            dto.add(consultarPessoaPorNomeDTO);
        }
        return dto;
    }

    @SneakyThrows
    private Pair<ConsultarPessoaPorNomeDTO.Unidade, ConsultarPessoaPorNomeDTO.PessoaDepartamentoFuncao> getUnidade(Node node) {

        Unidade unidade = getElementoFromNode(node.getNodeParent(), "Unidade");
        Departamento departamento = getElementoFromNode(node.getNodeParent(), "Departamento");
        Funcao funcao = getElementoFromNode(node.getNodeParent(), "Funcao");
        Pessoa pessoa = node.getVinculo().getPessoa();

        ConsultarPessoaPorNomeDTO.Unidade unidadeDTO = new ConsultarPessoaPorNomeDTO.Unidade(unidade.getId(), unidade.getNome());

        ConsultarPessoaPorNomeDTO.PessoaDepartamentoFuncao pessoaDepartamentoFuncao = new ConsultarPessoaPorNomeDTO.PessoaDepartamentoFuncao(
                criarPessoa(pessoa),
                criarDepartamento(departamento),
                criarFuncao(funcao));
        return Pair.of(unidadeDTO, pessoaDepartamentoFuncao);
    }

    private ConsultarPessoaPorNomeDTO.Funcao criarFuncao(Funcao funcao) {
        return new ConsultarPessoaPorNomeDTO.Funcao(funcao.getId(), funcao.getNome());
    }

    private ConsultarPessoaPorNomeDTO.Departamento criarDepartamento(Departamento departamento) {
        return new ConsultarPessoaPorNomeDTO.Departamento(departamento.getId(), departamento.getNome());
    }

    private ConsultarPessoaPorNomeDTO.Pessoa criarPessoa(Pessoa pessoa) {
        return new ConsultarPessoaPorNomeDTO.Pessoa(pessoa.getId(), pessoa.getNome());
    }

    @SneakyThrows
    @SuppressWarnings("unchecked")
    private <T> T getElementoFromNode(Node node, String mapeamento) {
        T expected = null;
        while (node != null && (expected = (T) node.getClass().getMethod("get" + mapeamento).invoke(node)) == null) {
            node = node.getNodeParent();
        }

        return expected;
    }
}
