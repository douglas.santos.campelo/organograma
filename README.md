# Projeto Organograma

Este é um projeto desafiador criado com o objetivo de avaliar a capacidade de um candidato à vaga de programador backend em refatorar um projeto em Java, aplicando as melhores práticas de desenvolvimento de software.

O projeto foi criado na versão 17 do Java e possui um único controller em "OrganogramaPessoasController.java" com a rota "/organograma/pessoas/consultar-por-nome/{{nome}}". Utiliza um Docker Compose para inicializar o MySQL e o Flyway para cuidar das migrações do banco de dados.

## Entidades

As entidades do projeto são:

- Departamento: possui um nome e um ID.
- Função: possui um nome e um ID.
- Pessoa: possui um nome e um ID.
- Unidade: possui um nome e um ID.
- Vínculo: possui um ID e está vinculado a uma pessoa.
- Node: relaciona as demais entidades e forma uma estrutura de grafo.
- Organograma: possui um ID e um atributo "ativa" que determina qual organograma está em uso.

O Node é a entidade central do projeto. Ele possui um relacionamento consigo mesmo, o que permite criar uma estrutura de grafo. Cada nó representa uma unidade, departamento, função ou vínculo. Ex.:

```plantuml
@startmindmap

* Node Unidade 1
** Node Departamento 1
*** Node Função 1
**** Node Vinculo 1
**** Node Vinculo 2
**** Node Vinculo 3
*** Node Função 2
**** Node Vinculo 4
**** Node Vinculo 5
**** Node Vinculo 6
** Node Departamento 2
*** Node Função 3
**** Node Vinculo 7
**** Node Vinculo 8
**** Node Vinculo 9
*** Node Função 4
**** Node Vinculo 10
**** Node Vinculo 11
**** Node Vinculo 12

@endmindmap
```

## Desafio

A maior parte da lógica do projeto encontra-se no controller, o que não é uma boa prática de desenvolvimento de software. O desafio é refatorar o código, organizando-o em classes com responsabilidades bem definidas.

Além disso, o candidato deverá cobrir o projeto com testes para garantir o comportamento de cada nova classe criada.

## Critérios de Avaliação

Os candidatos serão avaliados com base nos seguintes critérios:

1. **Organização e limpeza do código:** O código é fácil de ler e de entender? As classes e métodos são bem organizados e têm responsabilidades claramente definidas? O candidato aplicou os princípios de código limpo na refatoração?

2. **Aplicação de padrões de projeto:** O candidato utilizou padrões de projeto apropriados para resolver os problemas apresentados? Esses padrões foram implementados de forma eficaz e contribuíram para a melhoria da estrutura do código?

3. **Aplicação dos princípios SOLID:** Os princípios SOLID foram aplicados corretamente e ajudaram a tornar o código mais flexível, compreensível e mantível?

4. **Domínio da linguagem:** O candidato demonstrou um bom domínio da linguagem Java? O código reflete a utilização eficaz dos recursos da linguagem?

5. **Testes:** Os testes cobrem todos os cenários possíveis? Eles asseguram o comportamento correto do código?

Estes critérios são importantes para garantir que o código seja de alta qualidade, fácil de manter e ampliar, além de estar livre de bugs. Esperamos ver uma solução bem pensada que demonstre o conhecimento e a compreensão do candidato sobre boas práticas de desenvolvimento de software.

## Como executar o projeto

Primeiro, é necessário ter o Docker instalado. Em seguida, execute o comando a seguir para inicializar o MySQL e o Flyway:

```bash
docker-compose -f src/main/docker/mysql.yml up -d
```

Depois de inicializar o banco de dados, você pode executar o projeto Java.

## Como testar o projeto

Escreva testes unitários para cada classe que você criar. Garanta que os testes cobrem todos os cenários possíveis e asseguram o comportamento correto do código.

---

## Instruções para Submissão

Para realizar e submeter este desafio, siga as instruções a seguir:

1. **Fork do Repositório:** Faça um fork deste repositório para a sua conta no GitLab. Se você não sabe como fazer isso, pode seguir [este guia](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#creating-a-fork).

2. **Clone o Repositório:** Clone o repositório para o seu ambiente de desenvolvimento local e faça suas alterações. Lembre-se de fazer commits regularmente para rastrear seu progresso.

3. **Refatoração:** Realize a refatoração do código seguindo as diretrizes e critérios descritos acima. Não se esqueça de incluir testes para assegurar que o código refatorado funciona corretamente.

4. **Adicionar Membro:** Quando você tiver concluído a refatoração, adicione o e-mail de quem lhe enviou este desafio como membro do seu repositório. Para fazer isso, vá para o seu repositório no GitLab e clique em "Settings" > "Members". Em "Invite member", digite o e-mail e escolha um nível de acesso (por exemplo, "Reporter"). Clique em "Invite" para enviar o convite.

Ao realizar esses passos, você nos permitirá revisar o código que você refatorou. Por favor, garanta que todos os commits e alterações necessárias estejam no repositório antes de nos adicionar como membro.

Esperamos que este desafio seja uma oportunidade interessante para você demonstrar suas habilidades como programador backend. Boa sorte!